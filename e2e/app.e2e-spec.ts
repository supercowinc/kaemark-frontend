import { KaemarkFrontendPage } from './app.po';

describe('kaemark-frontend App', () => {
  let page: KaemarkFrontendPage;

  beforeEach(() => {
    page = new KaemarkFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
