import { Injectable } from '@angular/core';

import { DataService } from './data-service';

@Injectable()
export class APIService {
  public static localhost = '/api/v1';

  private _header_url = APIService.localhost + '/rma_header/';
  private _label_status_url = APIService.localhost + '/pallet_header/';

  constructor (
    private dataService: DataService
  ) { }
  // Order Entry

  getRmaHeaderItems(id: number) {
    const url = this._header_url + id;
    return this.dataService.get(url);
  }

  getAccountingRmaInfo() {
    return this.dataService.get(this._header_url);
  }
}
