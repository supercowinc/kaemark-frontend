import { RouterModule } from '@angular/router';
import { AccountingComponent } from './accounting.component';
import { AccountingHomeComponent } from './accounting.home';
import { AccountingRmaComponent } from './rma/accounting.rma';
import { AccountingRmaDetailComponent } from './rma/accounting.rma.detail';

export const AccountingRoutes = RouterModule.forChild([
  { path: 'accounting', component: AccountingComponent, children: [
      { path: '', component: AccountingHomeComponent },
      { path: 'rma', component: AccountingRmaComponent },
      { path: 'rma/:id', component: AccountingRmaDetailComponent }
    ]
  }
]);
