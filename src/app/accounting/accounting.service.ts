import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class AccountingService {
  private _rma_header = APIService.localhost + '/rma_header/';
  private _accounting_rma_header_url = APIService.localhost + '/rma_header/accounting/';
  private _accounting_completed_rma_header_url = APIService.localhost + '/rma_header/accounting/complete/';
  private _rma_line_url = APIService.localhost + '/rma_line/';
  private _receiving_lines_url = APIService.localhost + '/rma_line/receiving/';


  public getAccountingRmaList() {
    return this.dataService.get(this._accounting_rma_header_url);
  }

  public getCompletedAccountingRmaList() {
    return this.dataService.get(this._accounting_completed_rma_header_url);
  }

  public getAccountingHeaderDetail(id: number) {
    const url = this._rma_header + id + '/';
    return this.dataService.get(url);
  }

  public getAccountingRmaHeaderItems(id: number) {
    const url = this._receiving_lines_url + id + '/';
    return this.dataService.get(url);
  }

  public putAccountingItems(rma_lines) {
    const url = this._rma_line_url;
    return this.dataService.put(url, rma_lines);
  }

  public putAccountingCreditMemo(rma_header, accounting_header_id) {
    const url = this._rma_header + accounting_header_id + '/';
    return this.dataService.put(url, rma_header);
  }

  constructor(private dataService: DataService) {}
}
