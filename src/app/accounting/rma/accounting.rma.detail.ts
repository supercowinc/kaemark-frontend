import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountingService } from '../accounting.service';

import { RmaHeader } from '../../models/rma/rma-header';
import { RmaLines } from '../../models/rma/rma-lines';

@Component({
  selector: 'app-accounting-rma-detail',
  templateUrl: './accounting.rma.detail.html'
})

export class AccountingRmaDetailComponent implements OnInit {
  private id: number;
  public rma_header = new RmaHeader();
  public rma_lines = new Array<RmaLines>();

  constructor(
    private service: AccountingService,
    private activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.getAccountingHeaderDetail(this.id).subscribe(
      res => {
        this.rma_header = res;
      },
      err => {
        alert(err);
      }
    );
    this.service.getAccountingRmaHeaderItems(this.id).subscribe(
      res => {
        this.rma_lines = res;
      },
      err => {
        alert(err);
      }
    );
  }

  public onAccountingRmaLineSubmit() {
    this.service.putAccountingItems(this.rma_lines).subscribe(
      res => {
        alert('You have successfully submitted your updates!');
      },
      err => {
        alert(err);
      }
    );
  }

  public onAccountingCreditMemo() {
    const accounting_header_id = this.rma_header.id;
    this.service.putAccountingCreditMemo(this.rma_header, accounting_header_id).subscribe(
      res => {
        alert('You have successfully saved the credit memo number!');
      },
      err => {
        alert('That Credit Memo Number does not exist!');
      }
    );
  }

}
