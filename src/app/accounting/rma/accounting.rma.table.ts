import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AccountingService } from '../accounting.service';
import { RmaHeader } from '../../models/rma/rma-header';

@Component({
  selector: 'app-rma-accounting-table',
  templateUrl: './accounting.rma.table.html'
})

export class AccountingRmaTableComponent implements OnInit {
  public id: number;
  public rma_headers = new Array<RmaHeader>();
  private completed_rma_headers = new Array<RmaHeader>();

  constructor(
    private _router: Router,
    private service: AccountingService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.getAccountingRmaList().subscribe(
      res => {
        this.rma_headers = res;
      },
      err => {
        alert(err);
      }
    );
    this.service.getCompletedAccountingRmaList().subscribe(
      res => {
        this.completed_rma_headers = res;
      }
    );
  }

  public OnRmaHeaderClick(rma_header) {
    this._router.navigate(['./', rma_header.id], {relativeTo: this.activatedRoute});
  }

  public OnCompletedRmaHeaderClick(completed_rma_header) {
    this._router.navigate(['./', completed_rma_header.id], {relativeTo: this.activatedRoute});
  }
}
