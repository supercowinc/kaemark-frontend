import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AccountingService } from '../accounting.service';

@Component({
  selector: 'app-add-rma',
  templateUrl: './accounting.rma.html'
})

export class AccountingRmaComponent implements OnInit {
  private psi: number;

  constructor(
    private _router: Router,
    private activatedRoute: ActivatedRoute,
    private service: AccountingService
  ) {
    this.activatedRoute.params.subscribe(p => {
      const psi = p['psi'];
      if (psi) {
        this.psi = psi;
      }
    });
  }

  public ngOnInit() {
    this.service.getAccountingRmaList().subscribe(
      res => { },
      err => {
        alert(err);
      }
    );
  }



}
