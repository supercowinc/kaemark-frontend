import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class ReceivingService {
  private _header_url = APIService.localhost  + '/rma_header/';
  private _lines_url = APIService.localhost  + '/rma_line/';
  private _receiving_header_url = APIService.localhost  + '/rma_header/receiving/';
  private _receiving_lines_url = APIService.localhost  + '/rma_line/receiving/';
  private _receiving_complete = APIService.localhost  + '/rma_header/receiving/complete/';

  public getReceivingRmaList() {
    return this.dataService.get(this._receiving_header_url);
  }

  public getReceivingCompleteRmaList() {
    return this.dataService.get(this._receiving_complete);
  }

  public getReceivingHeaderDetail(id: number) {
    const url = this._header_url + id + '/';
    return this.dataService.get(url);
  }

  public getReceivingRmaHeaderItems(id: number) {
    const url = this._receiving_lines_url + id + '/';
    return this.dataService.get(url);
  }

  public ReceivedItemPut(received_items, received_items_id) {
    const url = this._lines_url;
    return this.dataService.put(url, received_items);
  }

  public ReceivedHeaderStatus(status, received_header_id) {
    const url = this._header_url + received_header_id + '/';
    return this.dataService.patch(url, status);
  }

  constructor(private dataService: DataService) {}
}
