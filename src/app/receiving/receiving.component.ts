import { Component } from '@angular/core';

@Component({
  selector: 'app-receiving-component',
  template: `<router-outlet></router-outlet>`
})

export class ReceivingComponent { }
