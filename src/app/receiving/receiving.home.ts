import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SalesInvoiceHeader } from '../models/sales/sales-invoice-header';

@Component({
  selector: 'app-receiving-home',
  templateUrl: './receiving.home.html'
})

export class ReceivingHomeComponent {

  private sales_invoice_header: SalesInvoiceHeader = new SalesInvoiceHeader();

  constructor(
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  onReceivingPsiSubmit() {
    this._router.navigate(['./rma', this.sales_invoice_header.no_field], { relativeTo: this.activatedRoute });
  }
}
