import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class OeService {

  private _invoice_url = APIService.localhost + '/sales_invoice_header/';
  private _header_url = APIService.localhost + '/rma_header/';
  private _header_detail = APIService.localhost + '/rma_header/detail/';
  private _items_url = APIService.localhost + '/sales_invoice_line/';
  private _lines_url = APIService.localhost + '/rma_line/';
  private _oe_lines_url = APIService.localhost + '/rma_line/oe/';
  private _messages_url = APIService.localhost + '/rma_message/';

  // POSTS
  public OeRmaHeaderPost(rma_header) {
    return this.dataService.post(this._header_url, rma_header);
  }

  public OeRmaLinePost(post_lines) {
    return this.dataService.post(this._lines_url, post_lines);
  }

  public PostRmaLine(rma_lines) {
    return this.dataService.post(this._lines_url, rma_lines);
  }

  public OePostMessage(messages) {
    return this.dataService.post(this._messages_url, messages);
  }

  // GETS
  public OeGetItems(psi: number) {
    const url = this._items_url + 'PSI' + psi + '/';
    return this.dataService.get(url);
  }

  public OeGetInvoiceInfo(psi: number) {
    const url = this._invoice_url + 'PSI' + psi + '/';
    return this.dataService.get(url);
  }

  public OeGetRmaList() {
    return this.dataService.get(this._header_url);
  }

  public OeGetRmaHeader(id: number) {
    const url = this._header_url + id + '/';
    return this.dataService.get(url);
  }

  public OeGetRmaHeaderItems(id: number) {
    const url = this._header_detail + id + '/';
    return this.dataService.get(url);
  }

  public OeGetRmaInvoiceItems(psi: number) {
    const url = this._oe_lines_url + psi + '/';
    return this.dataService.get(url);
  }

  public OeGetMessages() {
    const url = this._messages_url;
    return this.dataService.get(url);
  }

  // PUTS
  public OePutAllItemsFinal(credited_items) {
    const url = this._lines_url;
    return this.dataService.patch(url, credited_items);
  }

  public OeRmaHeaderPut(rma_header: any) {
    const url = this._header_url + rma_header.id + '/';
    return this.dataService.patch(url, rma_header);
  }

    constructor(private dataService: DataService) {}
}
