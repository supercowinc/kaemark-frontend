import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { OeService } from '../../oe.service';

import { RmaHeader } from '../../../models/rma/rma-header';

@Component({
  selector: 'app-oe-rma-detail',
  templateUrl: './oe.rma-detail.html'
})

export class OeRmaDetailComponent implements OnInit {
  public rma_header = new RmaHeader();
  public rma_header_id: number;

  constructor(
    private service: OeService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.rma_header_id = id;
      }
    });
  }

  ngOnInit() {
    this.service.OeGetRmaHeader(this.rma_header_id).subscribe(
      res => {
        this.rma_header = res;
      },
      err => {
        alert(err);
      }
    );
  }

  public oeRmaHeaderSubmit() {
    this.service.OeRmaHeaderPut(this.rma_header).subscribe(
      res => {
        alert('Your changes have been saved.');
      },
      err => {
        alert(err);
      }
    );
  }
}
