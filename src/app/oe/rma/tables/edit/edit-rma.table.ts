import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { OeService } from '../../../oe.service';

import { RmaLines } from '../../../../models/rma/rma-lines';

@Component({
  selector: 'app-oe-edit-rma-table',
  templateUrl: './edit-rma.table.html'
})

export class OeEditRmaTableComponent implements OnInit {
  @Input() rma_header_id: number;
  public rma_lines = new Array<RmaLines>();

  constructor(
    private service: OeService,
    private activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.rma_header_id = id;
      }
    });
  }

  ngOnInit() {
    this.service.OeGetRmaHeaderItems(this.rma_header_id).subscribe(
      res => {
        this.rma_lines = res;
      },
      err => {
        alert(err);
      }
    );
  }

  public onOeLineItemReturnedSubmit() {
    const credited_items = this.rma_lines.filter(r => r.amount_credited != null);
    const credited_ids = [];
    const received_items_id = credited_ids.push(credited_items.forEach(x => x.id));
    this.service.OePutAllItemsFinal(credited_items).subscribe(
      res => {
        console.log(res);
      },
      err => {
        alert(err);
      }
    );
  }
}
