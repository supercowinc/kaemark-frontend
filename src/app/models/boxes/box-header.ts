export class PalletLines {
  public id: number;
  public pallet_number: number;
  public box_info_key: string;
  public sales_order_number: string;
  public box_description: string;
  public box_staged: string;
  public box_staged_timestamp: string;
  public loaded_as_pallet: boolean;
  public loaded_timestamp: string;
  public loaded_person: string;
  public box_staged_location: string;
  public package_weight: string;
  public shipping_classification: string;
  public package_length: string;
  public package_width: string;
  public package_height: string;
  public voided_box: string;
  public production_label_timestamp: string;
  public production_label_person: string;
  public pallet_person: string;
  public testcount_timestamp: string;
  public testcount_person: string;
  public testcount_as_pallet: boolean;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
