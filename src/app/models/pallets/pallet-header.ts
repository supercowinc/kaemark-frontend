export class PalletHeader {
  public id: number;
  public pallet_length: string;
  public pallet_width: string;
  public pallet_height: string;
  public pallet_shipping_class: string;
  public pallet_weight: string;
  public pallet_label_printed: boolean;
  public number_of_boxes: number;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
