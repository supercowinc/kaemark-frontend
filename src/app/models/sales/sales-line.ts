export class SalesLine {
  public box_info_key: string;
  public no_field: string;
  public description: string;
  public description_2: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
