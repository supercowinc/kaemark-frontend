export class SalesHeader {
  public no_field:                  string;
  public kaemark_truck_no_field:    string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
