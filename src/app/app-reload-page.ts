import { Component, OnInit } from '@angular/core';

import { ShippingService } from './shipping/shipping.service';

import { PalletLines } from './models/boxes/box-header';
import { PalletHeader } from './models/pallets/pallet-header';
import { SalesLine } from './models/sales/sales-line';

declare var PrintPalletLabel: any;

@Component({
  selector: 'app-reload-page',
  template: ``
})

export class ReloadPageComponent implements OnInit {
  public pallets = new Array<PalletHeader>();
  private pallet_lines = new Array<PalletLines>();
  private item_details = new Array<SalesLine>();
  private pallet_number = this.pallet_lines.filter(x => x.pallet_number);
  private item_numbers = [];
  private barcode: string;
  public id: number;

  constructor (private service: ShippingService) { }

  public callingFunctionForLabelStatus() {
    this.service.getPalletLabelStatus().subscribe(
      pallet_label_res => {
        const print_boxes = pallet_label_res.filter(x => x.pallet_label_printed === false);
        if (print_boxes.length > 0) {
          this.pallets = print_boxes;
          print_boxes.forEach(pallet => {
            this.service.getBoxesAccordingToPallet(pallet.id).subscribe(
              get_boxes_res => {
                this.pallet_lines = get_boxes_res;
                get_boxes_res.forEach(pl => {
                  this.service.getItemDetail(pl.box_info_key).subscribe(
                    item_res => {
                        if (item_res[0] !== undefined) {
                          if (this.item_details.length !== this.pallet_lines.length) {
                            this.item_details.push(item_res[0]);
                          }
                        }
                    },
                    err => {
                      console.error(err);
                    }
                  );
                });
              },
              err => {
                console.error(err);
              }
            );
          });
          PrintPalletLabel(this.pallets, this.pallet_lines, this.item_details);
          //Object.getOwnPropertyNames(this.pallets).forEach(function (prop) {
            //delete this.pallets[prop];
          //});
          if (this.pallet_lines.length > 0) {
            this.pallets.length = 0;
            this.pallet_lines.length = 0;
            this.item_details.length = 0;
          }
        } else {
          console.log('There were no pallets to print.');
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  ngOnInit() {
    setInterval(() => this.callingFunctionForLabelStatus(), 5000);
  }
}
