import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home';
import { ReloadPageComponent } from './app-reload-page';

export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'reloadpage', component: ReloadPageComponent },
    { path: '', loadChildren: 'app/accounting/accounting.module#AccountingModule'},
    { path: '', loadChildren: 'app/receiving/receiving.module#ReceivingModule'},
    { path: '', loadChildren: 'app/oe/oe.module#OeModule' },
    { path: '', loadChildren: 'app/shipping/shipping.module#ShippingModule' },
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(routes);
