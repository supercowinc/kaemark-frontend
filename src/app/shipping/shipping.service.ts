import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class ShippingService {

  private _pallets_url = APIService.localhost + '/pallet_header/';
  private _items_for_boxing = APIService.localhost + '/sales_invoice_line/';
  private _box_url = APIService.localhost + '/pallet_line/';
  private _get_boxes_url = APIService.localhost + '/pallet_line/pallet/';
  private _get_boxes_info_key_url = APIService.localhost + '/pallet_line/box_info/'
  private _get_this_box_url = APIService.localhost + '/pallet_line/shipping/box/';
  private _sales_header_url = APIService.localhost + '/sales_header/';
  private _sales_line_url = APIService.localhost + '/sales_line/';
  private _loading_location_url = APIService.localhost + '/pallet_line/loading_location/';

// POST Requests CHANGE TO PUTS

  public putNumberOfBoxesOnPallet(pallet) {
    const url = this._pallets_url + pallet.id + '/';
    return this.dataService.patch(url, pallet);
  }

  public putPalletLabelPrintedFalse(pallets) {
    const url = this._pallets_url + pallets.id + '/';
    return this.dataService.patch(url, pallets);
  }

  public putBoxTestedTimeStamp(boxTested) {
    const url = this._box_url + boxTested.id + '/';
    return this.dataService.patch(url, boxTested);
  }

  public putLoadIsTrue(boxes) {
    const url = this._box_url;
    return this.dataService.patch(url, boxes);
  }

  public putNullPalletNumber(box) {
    const url = this._get_boxes_url + box.id + '/';
    return this.dataService.put(url, box);
  }

  public putNullPalletNumberOnBox(pallet_line) {
    const url = this._box_url + pallet_line.id + '/';
    return this.dataService.put(url, pallet_line);
  }

  public putPalletNumber(box) {
      const url = this._box_url + box.id + '/';
      return this.dataService.patch(url, box);
  }


  public ShippingCreatePallet(pallet) {
    const url = this._pallets_url;
    return this.dataService.post(url, pallet);
  }

  public ShippingAddBox(box) {
    const url = this._box_url;
    return this.dataService.post(url, box);
  }
// GET Requests
  public getBoxDetail(box_info_key, box_no) {
    const url = this._get_this_box_url + box_info_key + '/' + box_no + '/';
    return this.dataService.get(url);
  }

  public ShippingGetPalletList() {
    const url = this._pallets_url;
    return this.dataService.get(url);
  }

  public getPalletDetail(id) {
    const url = this._pallets_url + id + '/';
    return this.dataService.get(url);
  }

  public getItemDetail(box_info_key) {
    const url = this._sales_line_url + box_info_key + '/';
    return this.dataService.get(url);
  }

  public getBoxesAccordingToPallet(pallet_number) {
    const url = this._get_boxes_url + pallet_number + '/';
    return this.dataService.get(url);
  }

  public getBoxesAccordingToBoxInfoKey(id) {
    // change this url for getting the boxes by box_info_key
    const url = this._get_boxes_info_key_url + id + '/';
    return this.dataService.get(url);
  }

  public getSalesHeaderInfo(no_field: number) {
    const url = this._sales_header_url + no_field + '/';
    return this.dataService.get(url);
  }

  public getPalletLabelStatus() {
    return this.dataService.get(this._pallets_url);
  }

  public getLoadingLocation(id) {
    const url = this._loading_location_url + id + '/';
    return this.dataService.get(url);
  }
  constructor(private dataService: DataService) {}
}
