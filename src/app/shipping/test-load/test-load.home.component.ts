import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
// Models
import { PalletHeader } from '../../models/pallets/pallet-header';
import { PalletLines } from '../../models/boxes/box-header';
import { SalesHeader } from '../../models/sales/sales-header';
// Services
import { ShippingService } from '../shipping.service';

@Component({
  selector: 'app-test-load-home',
  templateUrl: './test-load.home.component.html'
})

export class TestLoadHomeComponent implements OnInit {
  private pallet = new PalletHeader();
  private box = new PalletLines();
  private boxes = new Array<PalletLines>();
  private sales_header = new SalesHeader();
  private barcode: string;
  private person: string;
  private sales_order_number: string;
  private sales_order_numbers = [];

  constructor(
    private service: ShippingService
  ) { }

  ngOnInit() { }

  public RetrieveData(box) {
    if (box.box_info_key.includes('.')) {
      const split_num = box.box_info_key.split('.');
      const box_info_key = split_num[0];
      const box_no = split_num[1];
      this.service.getBoxDetail(box_info_key, box_no).subscribe(
        get_box_detail_res => {
          const box_detail = get_box_detail_res[0];
          if (get_box_detail_res.length > 0) {
            this.sales_order_number = box_detail.sales_order_number;
            this.service.getSalesHeaderInfo(box_detail.sales_order_number).subscribe(
              sales_header_res => {
                if (sales_header_res.length > 0) {
                  this.sales_header = sales_header_res;
                  this.sales_header.kaemark_truck_no_field = sales_header_res.kaemark_truck_no_field;
                } else {
                  console.log('This doesn\'t belong to the same BOL.');
                }
              },
              err => {
                console.error(err);
              }
            );
            this.service.getPalletDetail(box_detail.pallet_number).subscribe(
              pallet_detail_res => {
                this.pallet = pallet_detail_res;
                this.service.getBoxesAccordingToPallet(box_detail.pallet_number).subscribe(
                  get_boxes_res => {
                    this.boxes = get_boxes_res;
                  },
                  err => {
                    console.error(err);
                  }
                );
              },
              err => {
                console.error(err);
              }
            );
          }
        },
        err => {
          console.error(err);
        }
      );
    } else {
      this.service.getPalletDetail(box.box_info_key).subscribe(
        second_pallet_detail_res => {
          this.pallet = second_pallet_detail_res;
          this.service.getBoxesAccordingToPallet(second_pallet_detail_res.id).subscribe(
            second_get_boxes_res => {
              this.boxes = second_get_boxes_res;
              second_get_boxes_res.forEach(res => {
                this.sales_order_numbers.push(second_get_boxes_res.sales_order_number);
              });
            },
            err => {
              console.error(err);
            }
          );
        }
      );
    }
  }

  public Test() {
    if (this.person === undefined) {
      alert('Please enter the shipping employee name that is currently testing the load.');
    } else {
      if (this.barcode.includes('.')) {
        const split_num = this.barcode.split('.');
        const box_info_key = split_num[0];
        const box_no = split_num[1];
        this.service.getBoxDetail(box_info_key, box_no).subscribe(
          third_get_box_detail_res => {
            if (third_get_box_detail_res[0].testcount_timestamp === null) {
              const boxTested = third_get_box_detail_res[0];
              boxTested.testcount_timestamp = moment().format();
              this.service.putBoxTestedTimeStamp(boxTested).subscribe();
              if (third_get_box_detail_res[0].sales_order_number === this.sales_order_number) {
                console.log('This box belongs with the pallet!');
              } else {
                this.service.getSalesHeaderInfo(third_get_box_detail_res[0].sales_order_number).subscribe(
                  truck_no_res => {
                    if (truck_no_res[0].kaemark_truck_no_field === this.sales_header.kaemark_truck_no_field) {
                      console.log(true);
                    } else {
                      alert('This Box did not belong to the same order or BOL as the one previously scanned!');
                    }
                  },
                  err => {
                    console.error(err);
                  }
                );
                console.log(false);
              }
            } else {
              alert('This box has already been tested!');
            }
          },
          err => {
            console.error(err);
          }
        );
      } else {
        this.service.getBoxesAccordingToPallet(this.barcode).subscribe(
          fourth_get_boxes_res => {
            if (fourth_get_boxes_res.length > 0) {
              fourth_get_boxes_res.forEach(res => {
                const result = this.sales_order_numbers.includes(fourth_get_boxes_res.sales_order_number);
                if (result === true) {
                  console.log('The boxes on this pallet belong to the same order as the previously scanned item!');
                  if (fourth_get_boxes_res.testcount_timestamp === null) {
                    fourth_get_boxes_res.testcount_as_pallet = true;
                    fourth_get_boxes_res.testcount_timestamp = moment().format();
                    fourth_get_boxes_res.testcount_person = this.person;
                    this.service.putBoxTestedTimeStamp(fourth_get_boxes_res).subscribe(
                      put_res => {
                        console.log(put_res);
                      },
                      err => {
                        console.error(err);
                      }
                    );
                  } else {
                    console.log('The boxes on this pallet have already been tested!');
                  }
                } else {
                  console.log('These boxes don\'t belong to the same order number as the previously scanned item!');
                }
              });

            } else {
              alert('There are no boxes on this pallet.');
            }
          },
          err => {
            console.error(err);
          }
        );
      }
    }
  }

  public LoadPallet() {
    if (this.person === undefined) {
      alert('Please enter the shipping employee name that is currently loading.');
    } else {
      if (this.boxes.filter(x => x.loaded_timestamp !== null)) {
        alert('This pallet and its boxes have already been loaded');
      } else {
        this.boxes.forEach(
          x => x.loaded_as_pallet = true,
          x => x.loaded_timestamp = moment().format(),
        );
        this.boxes.filter(x => x.loaded_person = this.person);
        this.service.putLoadIsTrue(this.boxes).subscribe(
          res => {
            console.log('Boxes are loaded');
          },
          err => {
            console.error(err);
          }
        );
      }
    }
  }
}
