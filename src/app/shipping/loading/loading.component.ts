import { Component, OnInit } from '@angular/core';

import { LoadingLocation } from '../../models/loading/loading-location';
import { PalletLines } from '../../models/boxes/box-header';

import { ShippingService } from '../shipping.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {
  private loading_location = new LoadingLocation();
  private pallet_lines = new Array<PalletLines>();

  constructor(
    private service: ShippingService,
  ) { }

  ngOnInit() {
  }

  public getSalesOrders(loading_location) {
    this.service.getLoadingLocation(loading_location.loading_location).subscribe(
      res => {
        console.log(res[0].order_no);
        if (res[0].order_no !== undefined) {
          this.service.getBoxesAccordingToBoxInfoKey(res[0].order_no).subscribe(
            box_res => {
              console.log(box_res);
              // this.pallet_lines = box_res;
              /*
              const pallet_numbers = [];
              this.pallet_lines.forEach(function(pl) {
              pallet_numbers.push(pl.pallet_number);
            });
            */
          },
          box_err => {
            console.error(box_err);
          }
        );
      }
    },
    err => {
      console.error(err);
    }
  );
}

}
