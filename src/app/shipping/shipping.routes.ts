import { RouterModule } from '@angular/router';
import { ShippingComponent } from './shipping.component';
import { ShippingHomeComponent } from './shipping.home';
import { PalletsHomeComponent } from './pallets/pallets.home.component';
import { PalletCreationHomeComponent } from './pallets/pallet-creation/pallet-creation.home.component';
import { TestLoadHomeComponent } from './test-load/test-load.home.component';
import { PalletDetailsComponent } from './pallets/pallet-details/pallet-details.component';
import { LoadingComponent } from './loading/loading.component';

export const ShippingRoutes = RouterModule.forChild([
  { path: 'shipping', component: ShippingComponent, children: [
      { path: '', component: ShippingHomeComponent },
      { path: 'pallets', component: PalletsHomeComponent },
      { path: 'pallets/pallet_creation', component: PalletCreationHomeComponent },
      { path: 'test_load', component: TestLoadHomeComponent },
      { path: 'load/details/:id', component: PalletDetailsComponent },
      { path: 'loading', component: LoadingComponent },
    ]
  }
]);
