import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShippingRoutes } from './shipping.routes';
import { ShippingService } from './shipping.service';

import { ShippingHomeComponent } from './shipping.home';
import { ShippingComponent } from './shipping.component';
import { PalletsHomeComponent } from './pallets/pallets.home.component';
import { PalletCreationHomeComponent } from './pallets/pallet-creation/pallet-creation.home.component';
import { TestLoadHomeComponent } from './test-load/test-load.home.component';
import { PalletDetailsComponent } from './pallets/pallet-details/pallet-details.component';
import { LoadingComponent } from './loading/loading.component';
import { AutofocusDirective } from './autofocus.directive';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ShippingRoutes
  ],
  declarations: [
    ShippingComponent,
    ShippingHomeComponent,
    PalletsHomeComponent,
    PalletCreationHomeComponent,
    TestLoadHomeComponent,
    PalletDetailsComponent,
    LoadingComponent,
    AutofocusDirective
  ],
  providers: [ShippingService]
})
export class ShippingModule { }
