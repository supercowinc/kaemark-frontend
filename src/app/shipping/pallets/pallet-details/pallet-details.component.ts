import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { PalletHeader } from '../../../models/pallets/pallet-header';
import { PalletLines } from '../../../models/boxes/box-header';

import { ShippingService } from '../../shipping.service';

@Component({
  selector: 'app-pallet-details',
  templateUrl: './pallet-details.component.html',
  styleUrls: ['./pallet-details.component.css']
})
export class PalletDetailsComponent implements OnInit {
  public pallets = new PalletHeader();
  public pallet_lines = new Array<PalletLines>();
  public box = new PalletLines();
  public pallet_number = this.pallet_lines.filter(x => x.pallet_number);
  public pallet_id: number;

  constructor(
    private service: ShippingService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.pallets.id = id;
        this.pallet_number = id;
      }
    });
  }

  ngOnInit() {
    this.service.getBoxesAccordingToPallet(this.pallet_number).subscribe(
      res => {
        this.pallet_lines = res;
        console.log(res);
      },
      err => {
        alert(err);
      }
    );
    this.service.getPalletDetail(this.pallets.id).subscribe(
      res => {
        this.pallets = res;
        this.pallet_id = res.id;
      },
      err => {
        console.error(err);
      }
    );
  }

  public addBoxToPallet(box) {
    if (box.box_info_key.includes('.')) {
      const split_num = box.box_info_key.split('.');
      const box_info_key = split_num[0];
      const box_no = split_num[1];
      this.service.getBoxDetail(box_info_key, box_no).subscribe(
        get_box_detail_res => {
          console.log(get_box_detail_res);
          if (get_box_detail_res.length > 0) {
            const pallet_line = get_box_detail_res[0];
            if (pallet_line.pallet_number !== null) {
              alert('This box already has been assigned to a pallet!');
            } else {
              pallet_line.pallet_timestamp = moment().format();
              pallet_line.pallet_number = this.pallet_id;
              this.service.putPalletNumber(pallet_line).subscribe(
                res => {
                  location.reload();
                },
                err => {
                  console.log(err);
                }
              );
              this.pallets.number_of_boxes = this.pallets.number_of_boxes + 1;
              this.service.putNumberOfBoxesOnPallet(this.pallets).subscribe(
                number_of_boxes_res => {
                  console.log(number_of_boxes_res);
                },
                number_of_boxes_err => {
                  console.error(number_of_boxes_err);
                }
              );
            }
          }
        },
        err => {
          console.error(err);
        }
      );
    }
  }

  public printPalletLabel() {
    this.pallets.pallet_label_printed = false;
    this.service.putPalletLabelPrintedFalse(this.pallets).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  public editPallet(pallets) {
    this.service.putNumberOfBoxesOnPallet(pallets).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }


   public removeBox(pallet_line) {
     this.pallets.number_of_boxes = this.pallets.number_of_boxes - 1;
     this.service.putNumberOfBoxesOnPallet(this.pallets).subscribe(
       res => {
         console.log(res);
       },
       err => {
         console.error(err);
       }
     );
     pallet_line.pallet_number = null;
     this.service.putNullPalletNumberOnBox(pallet_line).subscribe(
         res => {
             location.reload();
         },
         err => {
             console.error(err);
         }
     );
   }

}
