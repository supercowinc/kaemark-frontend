import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PalletHeader } from '../../models/pallets/pallet-header';
import { PalletLines } from '../../models/boxes/box-header';

import { ShippingService } from '../shipping.service';

@Component({
  selector: 'app-pallets-home',
  styles: ['./pallets.home.component.css'],
  templateUrl: './pallets.home.component.html'
})

export class PalletsHomeComponent implements OnInit {
  public id: number;
  public pallets = new Array<PalletHeader>();
  public pallet_lines = new Array<PalletLines>();
  public pallet = new PalletHeader();
  public pallet_id: number;


  constructor(
    private service: ShippingService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.ShippingGetPalletList().subscribe(
      res => {
        this.pallets = res;
      },
      err => {
        console.error(err);
      }
    );
  }

  public CreatePallet() {
    this.pallet.pallet_weight = String(0);
    this.service.ShippingCreatePallet(this.pallet).subscribe(
      res => {
        this.pallet_id = res.id;
        this._router.navigate(['/shipping/load/details/', res.id], {relativeTo: this.activatedRoute});
      },
      err => {
        console.error(err);
      }
    );

  }

  public OnPalletClick(pallet) {
    this._router.navigate(['/shipping/load/details/', pallet.id], {relativeTo: this.activatedRoute});
  }
}
