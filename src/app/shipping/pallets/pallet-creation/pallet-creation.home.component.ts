import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

import { ShippingService } from '../../shipping.service';

import { PalletHeader } from '../../../models/pallets/pallet-header';
import { PalletLines } from '../../../models/boxes/box-header';

@Component({
  selector: 'app-pallet-creation-home',
  styles: ['./pallet-creation.home.component.css'],
  templateUrl: './pallet-creation.home.component.html'
})

export class PalletCreationHomeComponent implements OnInit {
  public pallet = new PalletHeader();
  public box = new PalletLines();
  public pallet_lines = new Array<PalletLines>();
  public number_of_boxes = [];
  public id: number;
  @Input() pallet_id: number;

  constructor(
    private service: ShippingService,
    private activatedRoute: ActivatedRoute,
    private _router: Router,
  ) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {

  }

  public addBoxToPallet(box) {
    if (box.box_info_key.includes('.')) {
      const split_num = box.box_info_key.split('.');
      const box_info_key = split_num[0];
      const box_no = split_num[1];
      this.service.getBoxDetail(box_info_key, box_no).subscribe(
        res => {
          if (res.length > 0) {
            const pallet_line = res[0];
            if (pallet_line.pallet_number !== null) {
              alert('This box already has been assigned to a pallet!');
            } else {
              this.number_of_boxes.push(box.id);
              this.pallet_lines.push(pallet_line);
              pallet_line.pallet_number = this.pallet_id;
              pallet_line.pallet_timestamp = moment().format();
              this.service.putPalletNumber(pallet_line).subscribe(
                response => {
                  console.log('Put the pallet_number on the box');
                },
                err => {
                  console.log(err);
                }
              );
              box.box_info_key = "";
            }
          }
        },
        err => {
          console.error(err);
        }
      );
      console.log(this.pallet.number_of_boxes);
    } else {

    }
  }

  public addPalletInfo(pallet) {
    pallet.id = this.pallet_id;
    this.pallet.pallet_label_printed = false;
    this.pallet.number_of_boxes = this.number_of_boxes.length;
    this.service.putNumberOfBoxesOnPallet(this.pallet).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  public removeBox(box) {
    box.pallet_number = null;
    this.service.putNullPalletNumber(box).subscribe(
      res => {
        console.log(res);
      },
      err => {
        alert(err);
      }
    );
    this.pallet.number_of_boxes - 1;
    this.service.putNumberOfBoxesOnPallet(this.pallet).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

}
